<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.partials.head')

<body class="bg-white">
	@yield('content')
	@include('layouts.partials.footer')
</body>
</html>
