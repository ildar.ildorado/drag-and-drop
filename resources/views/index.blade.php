@extends('layouts.light')

@section('title', 'Drag And Drop')

@section('content')
    <ul id="taskList" class="list-group">
        <li class="list-group-item">test 1</li>
        <li class="list-group-item">test 2</li>
        <li class="list-group-item">test 3</li>
        <li class="list-group-item">test 4</li>
        <li class="list-group-item">test 5</li>
    </ul>
@endsection